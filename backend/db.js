const mysql=require('mysql2')
const pool=mysql.createPool({
    host: 'db',
    user: 'root',
    password: 'root',
    waitForConnections: 'true',
    connectionLimit: 10,
    database: 'my_db',
})

module.exports ={
    pool,
}