const { response, request } = require('express')
const express=require('express')
const db=require('../db')
const utils=require('../util')
const router=express.Router()

router.get('/displayPerson',(request, response) => {
    const query=`select id,firstname,lastname,email,password from person`
    db.pool.execute(query,(error,persons) => {
        response.send(utils.createResult(error,persons))
    })
})

router.post('/addPerson',(request,response) => {
    const {firstName,lastName,email,password} = request.body
    const query=`insert into person (firstName,lastName,email,password) values (?,?,?,?)`
    db.pool.execute(query,[firstName,lastName,email,password],(error,result) => {
        response.send(utils.createResult(error,result))
    })
})

router.put('updateMovie/:movie_id', (request, response) => {
    const { id } = request.params;
    const { firstName,lastName,email,password } = request.body
    const query = `update person set firstName = ?, lastName = ?, email = ?, password = ? where id = ?`;
    db.pool.execute(query, [firstName,lastName,email,password],(error,result) => {
        response.send(utils.createResult(error,result));
    })
})

router.delete('/deletePerson/:id',(request,response)=>{
    const { id }=request.params
    const query=`delete from person where id=?`
    db.pool.execute(query,[id],(error,result)=>{
        response.send(utils.createResult(error,result))
    })
})

module.exports=router
