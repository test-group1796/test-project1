const express = require('express')
const cors = require('cors')
const routePerson = require('./router/person')

const app = express()
app.use(cors('*'))
app.use(express.json())
app.use('/person', routePerson)

app.get('/', (request, response) => {
    response.send('running inside a container')
})

app.listen(3000, '0.0.0.0', () => {
    console.log('server started on port 3000')
})